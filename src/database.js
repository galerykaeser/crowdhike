const Sequelize = require('sequelize')

const { databaseEnv } = process.env
const { passwordEnv } = process.env
const { hostEnv } = process.env
const { usernameEnv } = process.env

const database = new Sequelize({
  database: databaseEnv,
  host: hostEnv,
  username: usernameEnv,
  password: passwordEnv,
  dialect: 'postgres',
  operatorsAliases: 0,
  define: {
    timestamps: false
  }
})

database.drop();
console.log("All tables dropped!");

const Users = database.define('users', {
  user_id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
  nickname: { type: Sequelize.STRING, allowNull: false },
  password: { type: Sequelize.STRING, allowNull: false }
})

const Comments = database.define('comments', {
  comments_id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
  message: { type: Sequelize.STRING, allowNull: false }
})

const Paths = database.define('paths', {
  path_id: { type: Sequelize.STRING, primaryKey: true },
  name: { type: Sequelize.STRING, allowNull: true }
})

const Locations = database.define('locations', {
  location_id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
  longitude: { type: Sequelize.REAL, allowNull: false },
  latitude : { type: Sequelize.REAL, allowNull: false },
  moment: { type: Sequelize.DATE, allowNull: false }
})

// Foreign keys
Paths.belongsTo(Users, {foreignKey: 'fk_user', allowNull: false})
Paths.belongsTo(Comments, {foreignKey: 'fk_comment', allowNull: true})
Locations.belongsTo(Paths, {foreignKey: 'fk_path', allowNull: false,
                            onDelete: 'cascade'})

async function populate_database() {
    const {user_id} = await Users.create({
        user_id: 1,
        nickname: "postgres",
        password: "postgres"
    })
    const {comments_id} = await Comments.create({
        message: "A nice short walk from MIT-huset to IKSU."
    })
    const path_id = "MIT-huset-to-IKSU"
    await Paths.create({
        path_id: path_id,
        name: "IKSU walk",
        fk_comment: comments_id,
        fk_user: user_id
    });
    await Locations.create({
        latitude: 63.820490650803244,
        longitude: 20.30890571409228,
        path: path_id,
        moment: "2021-10-25 15:47:58.031206+02",
        fk_path: path_id
    });
    await Locations.create({
        latitude: 63.820760745907755,
        longitude: 20.30889744102854,
        path: path_id,
        moment: "2021-10-25 15:48:01.031206+02",
        fk_path: path_id
    });
    await Locations.create({
        latitude: 63.8207461462386,
        longitude: 20.30959237838271,
        path: path_id,
        moment: "2021-10-25 15:48:04.031206+02",
        fk_path: path_id
    });
    await Locations.create({
        latitude: 63.82070964703256,
        longitude: 20.310692695860144,
        path: path_id,
        moment: "2021-10-25 15:48:07.031206+02",
        fk_path: path_id
    });
    await Locations.create({
        latitude: 63.82053810013075,
        longitude: 20.311569640616597,
        path: path_id,
        moment: "2021-10-25 15:48:10.031206+02",
        fk_path: path_id
    });
    await Locations.create({
        latitude: 63.82008185328627,
        longitude: 20.312248031843286,
        path: path_id,
        moment: "2021-10-25 15:48:13.031206+02",
        fk_path: path_id
    });
    await Locations.create({
        latitude: 63.81958544832527,
        longitude: 20.312587227456632,
        path: path_id,
        moment: "2021-10-25 15:48:16.031206+02",
        fk_path: path_id
    });
    await Locations.create({
        latitude: 63.81953799737053,
        longitude: 20.31262859310929,
        path: path_id,
        moment: "2021-10-25 15:48:17.031206+02",
        fk_path: path_id
    });
    await Locations.create({
        latitude: 63.81938104371735,
        longitude: 20.313778548969168,
        path: path_id,
        moment: "2021-10-25 15:48:20.031206+02",
        fk_path: path_id
    });
    await Locations.create({
        latitude: 63.81924963998634,
        longitude: 20.315267700442384,
        path: path_id,
        moment: "2021-10-25 15:48:23.031206+02",
        fk_path: path_id
    });
    await Locations.create({
        latitude: 63.819096334858706,
        longitude: 20.31653347919462,
        path: path_id,
        moment: "2021-10-25 15:48:26.031206+02",
        fk_path: path_id
    });
    await Locations.create({
        latitude: 63.818636414469864,
        longitude: 20.316624482895765,
        path: path_id,
        moment: "2021-10-25 15:48:29.031206+02",
        fk_path: path_id
    });
    await Locations.create({
        latitude: 63.818636414469864,
        longitude: 20.31702158995529,
        path: path_id,
        moment: "2021-10-25 15:48:32.031206+02",
        fk_path: path_id
    });

}

module.exports = {
  Users,
  Comments,
  Paths,
  Locations,
  database,
  populate_database
}