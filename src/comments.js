const express = require('express')

const { Comments } = require('./database')

const router = express.Router()

router.get('/find/:comment_id', async (req, res, next) => {
  const { comment_id } = req.params
  res.json(await Comments.findOne({
    attributes: ['comment_id', 'message'],
    where: { comment_id }
  }))
})

router.post('/send', async (req, res, next) => {
  try {
    const { comment_id, message } = req.body
    const { id } = await Comments.create({ comment_id, message })
    res.json({ success: true, id })
  } catch (error) {
    res.json({ success: false, error: error.message })
  }
})

router.delete('/:comment_id', async (req, res, next) => {
  try {
    const { comment_id } = req.params
    if (await Comments.destroy({ where: { comment_id: comment_id } })) {
      res.json({ success: true })
    }
  } catch (error) {
    res.json({ success: false, error: 'Invalid ID' })
   }
})

module.exports = router