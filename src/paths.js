const express = require('express')

const { Paths, Comments } = require('./database')

const router = express.Router()

router.get('/find/:path_id', async (req, res, next) => {
  const { path_id } = req.params
  res.json(await Paths.findOne({
    attributes: ['path_id', 'name', 'fk_user', 'fk_comment'],
    where: { path_id }
  }))
})

router.post('/send', async (req, res, next) => {
  try {
    const { path_id, name, fk_user, fk_comment } = req.body
    const { id } = await Paths.create({ path_id, name, fk_user, fk_comment})
    res.json({ success: true, id })
  } catch (error) {
    res.json({ success: false, error: error.message })
  }
})

router.put('/rename/:path_id', async (req, res, next) => {
  try {
    const { name, comment } = req.body
    const {comments_id: comments_id, message: message} = await Comments.create({ message: comment })

    const { path_id } = req.params
    var selector = { 
      where: { path_id: path_id }
    };
    await Paths.update({name: name, fk_comment: comments_id}, selector)
    return res.json({ success: true, path_id })
  } catch (error) {
    res.json({ success: false, error: error.message })
  }
  res.json({ error: 'Invalid ID' })
})


router.delete('/:path_id', async (req, res, next) => {
  try {
    const { path_id } = req.params
    if (await Paths.destroy({ where: { path_id: path_id } })) {
      res.json({ success: true })
    }
  } catch (error) {
    res.json({ success: false, error: 'Invalid ID' })
   }
})
module.exports = router