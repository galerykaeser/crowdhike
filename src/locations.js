const express = require('express')

const { Locations, Paths, Comments } = require('./database')

const router = express.Router()

router.get('/find/:location_id', async (req, res, next) => {
  const { location_id } = req.params
  res.json(await Locations.findOne({
    attributes: ['location_id', 'latitude', 'longitude', 'moment', 'fk_path'],
    where: { location_id }
  }))
})

router.get('/findall', async (req, res, next) => {
  res.json(await Locations.findAll({
    include: [
      {
        model: Paths,
        required: true,
        include: [
          {
            model: Comments,
            required: true
          }
        ]
      }
    ],
    order: [
      ['moment', 'ASC']
    ]
  }))
})

router.post('/send', async (req, res, next) => {
  try {
    const {latitude, longitude, moment, fk_path } = req.body
    const { id } = await Locations.create({ latitude, longitude, moment, fk_path })
    res.json({ success: true, id })
  } catch (error) {
    res.json({ success: false, error: error.message })
  }
})

router.delete('/:location_id', async (req, res, next) => {
  try {
    const { location_id } = req.params
    if (await Locations.destroy({ where: { location_id: location_id } })) {
      res.json({ success: true })
    }
  } catch (error) {
    res.json({ success: false, error: 'Invalid ID' })
   }
})

module.exports = router