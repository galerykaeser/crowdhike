const express = require('express')

const { Users } = require('./database')

const router = express.Router()

router.get('/find/:user_id', async (req, res, next) => {
  const { user_id } = req.params
  res.json(await Users.findOne({
    attributes: ['user_id', 'nickname', 'password'],
    where: { user_id }
  }))
})

router.get('/findall', async (req, res, next) => {
  res.json(await Users.findAll({
    attributes: ['user_id', 'nickname', 'password']
  }))
})

router.post('/send', async (req, res, next) => {
  try {
    const { user_id, nickname, password } = req.body
    const { id } = await Users.create({ user_id, nickname, password })
    res.json({ success: true, id })
  } catch (error) {
    res.json({ success: false, error: error.message })
  }
})

router.delete('/:user_id', async (req, res, next) => {
  try {
    const { user_id } = req.params
    if (await Users.destroy({ where: { user_id: user_id } })) {
      res.json({ success: true })
    }
  } catch (error) {
    res.json({ success: false, error: 'Invalid ID' })
   }
})

module.exports = router