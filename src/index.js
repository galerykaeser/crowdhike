require('dotenv').config()
const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')

const { database, populate_database } = require('./database')

const port = process.env.SERVER_PORT || 3000

const app = express()
app.use(cors())
app.use(bodyParser.json())

app.use('/', (req, res, next) => {
  req.user_id = app.use('/users', require('./users'))
  next()
})

app.use('/', (req, res, next) => {
  req.user_id = app.use('/locations', require('./locations'))
  next()
})

app.use('/', (req, res, next) => {
  req.user_id = app.use('/paths', require('./paths'))
  next()
})

app.use('/', (req, res, next) => {
  req.user_id = app.use('/comments', require('./comments'))
  next()
})

database.sync({ force: true }).then(() => {
  app.listen(port, async () => {
    console.log(`Listening on port ${port}`);
    await populate_database();
    console.log("API READY !")
  })
})