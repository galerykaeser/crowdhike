import React from 'react';
import { Polyline } from 'react-native-maps';
import * as GlobalFunction from './GlobalFunction';

/**
 * Display every existing path
 */
export default class PathDisplayer extends React.Component {
	constructor(props) {
		super(props);
		// Allow to give parameter to this function
		this.handlePathSelection = this.handlePathSelection.bind(this);
	}

	// Update the user location
	updateLocation = async () => {
		try {
			const { latitude, longitude } = await GlobalFunction.getLocationAsync();
			this.props.updateLocation(latitude, longitude, 0.0009, 0.0009);
		} catch (err) {
			console.log(err);
		}
	}

	// onPress event handler for each path
	handlePathSelection = (pathId, pathName, pathComment, pathCoordinate) => {
		this.selectedPath_interval = setInterval(() => {
			this.updateLocation();
		}, 8000);
		this.props.setSelectedPath([{ key: pathId, value: { name: pathName, comment: pathComment, coordinate: pathCoordinate }, interval: this.selectedPath_interval }]);
	}

	// Mock function to do nothing when a path is already selected (avoid multiple interval definition)
	avoidPathSelection = (pathId, pathName, pathComment, pathCoordinate) => { }

	// Return a random color with format 'rgb(r, g, b)';
	randomColor = () => {
		return 'rgb(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ')';
	}

	// Render all the paths
	render() {
		const isCreatingPath = this.props.isCreatingPath();

		// Value if there is a path selected
		let allPath = this.props.getSelectedPath();
		let onPressHandler = this.avoidPathSelection;

		// Value if there is no path selected
		if (allPath == null) {
			allPath = this.props.getAllPath();
			onPressHandler = this.handlePathSelection;
		}

		if (isCreatingPath) {
			return (null);
		} else {
			return (
				<>
					{allPath.map(({ key, value }) => {
						const pathId = key;
						const pathName = value.name;
						const pathComment = value.comment;
						const pathCoordinate = value.coordinate;
						return (<Polyline
							key={pathId}
							name={pathName}
							comment={pathComment}
							coordinates={pathCoordinate}
							lineDashPattern={[1]}
							strokeWidth={4}
							strokeColor={this.randomColor()}
							onPress={() => onPressHandler(pathId, pathName, pathComment, pathCoordinate)}
							tappable={true}
						/>)
					})}
				</>
			);
		}
	}
}