import * as Location from 'expo-location';

// API URL
export var API_URL = "<API URL>";

// Update the location in the state variable.
export async function getLocationAsync() {
	try {
		await Location.requestForegroundPermissionsAsync();
		let location = await Location.getCurrentPositionAsync({ accuracy: Location.Accuracy.BestForNavigation });
		const { latitude, longitude } = location.coords;
		return ({ latitude: latitude, longitude: longitude, errorMessage: "" });
	} catch (err) {
		return ({ errorMessage: "Unable to access location. Check permission and gps activation." });
	}
}