import React from 'react';
import {StyleSheet, Text, View } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

/**
 * Render an error message in the screen of the app.
 * err: the error message to display.
 */
export default class ErrorManager extends React.Component {
    render(){
        const errorMessage = this.props.err;
        if(errorMessage != "") {
            return (
                <View style={styles.errorView}>
                    <FontAwesome style={styles.icons} size={'30'} name={'exclamation-circle'} color={'orange'}/>
                    <Text style={{textAlign: 'center'}}>{this.props.err}</Text>
                </View>
            );
        } else {
            return null;
        }
    }
}

const styles = StyleSheet.create({
    errorView:{
        position: 'absolute',
        alignItems: 'center',
        textAlign: 'center',
        bottom: 500,
        backgroundColor: 'white',
        borderRadius: 3,
        elevation: 5,
        margin: 20,
    },
    icons:{
        fontSize: 30,
    }
});