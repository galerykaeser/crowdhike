import React from 'react';
import { Polyline } from 'react-native-maps';
/***********************************************************************************************/

/**
 * Display the currently created path.
 */
export default class NewPathDisplayer extends React.Component {

	// Main screen
	render() {
		const path = this.props.getCurrentPath();
		return (
			<Polyline
				coordinates={path}
				lineDashPattern={[1]}
				strokeWidth={3}
			/>
		);
	}
}