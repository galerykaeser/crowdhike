import React from 'react';
import { StyleSheet, Text, View, Pressable, TextInput } from 'react-native';
import * as GlobalFunction from './GlobalFunction';
import * as Network from 'expo-network';
import * as Crypto from 'expo-crypto';

/**
 * Bottom view to create and manage path.
 */
export default class PathCreator extends React.Component {
	state = {
		isValidatePressed: false,
		name: "",
		comment: "",
	};

	/**
	 * Create a path and set a timer to send the location
	 * Called when user click on button "Create a path"
	 */
	createPath = async () => {
		// Hash both ip addr and current timestamp to create a path id
		try {
			const ipAddr = await Network.getIpAddressAsync();
			const currentTimestamp = Date.now().toString();
			const pathId = await Crypto.digestStringAsync(Crypto.CryptoDigestAlgorithm.SHA256, ipAddr + currentTimestamp);
			this.props.updatePathId(pathId);
			this.setState({ errorMessage: '' });
		} catch (err) {
			this.setState({ errorMessage: 'Unable to genereate a path identifier.' });
		}

		// Send a query with data to create the path
		const data = {
			path_id: this.props.pathId(),
			fk_user: 1, // default user
			name: null  // name is given at the end of the path
		}

		// Send the data
		fetch(GlobalFunction.API_URL + "/paths/send", {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(data),
		})
			.then(response => response.text()).then((responseJson) => {
				this.props.setIsCreatingPath(true);
			}).catch(error => console.log("Error : " + error))

		// Create a interval to send the location to the API
		this._data_interval = setInterval(() => {
			this.sendData();
		}, 3000);
	}

	/**
	 * Cancel a path => delete it and all its locations
	 * Called when user press the "Cancel" button
	 */
	cancelPath = () => {
		// Remove the interval
		clearInterval(this._data_interval);

		// delete the path and the location inserted
		fetch(GlobalFunction.API_URL + "/paths/" + this.props.pathId(), {
			method: 'DELETE',
			headers: {
				'Content-Type': 'application/json',
			}
		})
			.then(response => response.text()).then((responseJson) => {
				this.props.setIsCreatingPath(false);
				this.props.resetCurrentPath();
				this.props.updateAllPath();
			}).catch(error => console.log("Error : " + error))
	}

	/**
	 * Called when user press the "Validate" button
	 */
	validatePath = () => {
		// Remove the interval
		clearInterval(this._data_interval);
		this.setState({ isValidatePressed: true })
		this.props.setIsCreatingPath(false);
	}

	/**
	 * Send the location data
	 */
	sendData = async () => {
		try {
			const { latitude, longitude } = await GlobalFunction.getLocationAsync();
			const data = { latitude: latitude, longitude: longitude, moment: Date.now(), fk_path: this.props.pathId() }
			fetch(GlobalFunction.API_URL + "/locations/send", {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify(data),
			});
			this.props.updateLocation(latitude, longitude);
			this.props.pushCurrentPath(latitude, longitude);
		} catch (err) {
			console.log(err);
		}
	}

	/**
	 * Send the name and comment data
	 */
	sendAdditionalData = () => {
		// Send query with path name and comment
		data = {
			name: this.state.name,
			comment: this.state.comment
		}

		fetch(GlobalFunction.API_URL + "/paths/rename/" + this.props.pathId(), {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(data),
		})
			.then(response => response.text()).then((responseJson) => {
				this.setState({ isValidatePressed: false });

				// Display every available path
				this.props.resetCurrentPath();
				this.props.updateAllPath();
			}).catch(error => console.log("Error : " + error))
	}

	handleName = (name) => {
		this.setState({ name: name });
	}

	handleComment = (comment) => {
		this.setState({ comment: comment });
	}

	// Main screen
	render() {
		if (this.state.isValidatePressed) {
			return (
				<View style={styles.bottomView}>
					<TextInput
						style={styles.textInput}
						onChangeText={this.handleName}
						placeholder="Give a name to the path"
						maxLength={255}
						multiline={true}
					/>
					<TextInput
						style={styles.textInput}
						onChangeText={this.handleComment}
						placeholder="Write a comment to describe this path"
						maxLength={255}
						multiline={true}
					/>
					<Pressable onPress={this.sendAdditionalData} style={styles.validatePathButton}>
						<Text style={styles.textButton}>Send the additional data</Text>
					</Pressable>
				</View>
			);
		} else if (this.props.getIsCreatingPath()) {
			return (
				<View style={styles.bottomView}>
					<View style={{ flexDirection: 'row' }}>
						<Pressable onPress={this.cancelPath} style={styles.cancelPathButton}>
							<Text style={styles.textButton}>Cancel</Text>
						</Pressable>
						<Pressable onPress={this.validatePath} style={styles.validatePathButton}>
							<Text style={styles.textButton}>Validate</Text>
						</Pressable>
					</View>
				</View>
			);
		} else {
			return (
				<View style={styles.bottomView}>
					<Pressable onPress={this.createPath} style={styles.createPathButton}>
						<Text style={styles.textButton}>Create a path</Text>
					</Pressable>
				</View>
			);
		}
	}
}

const styles = StyleSheet.create({
	bottomView: {
		position: 'absolute',
		bottom: 0,
		width: '100%',
		backgroundColor: 'white',
		borderRadius: 3,
		elevation: 10,
		justifyContent: 'space-evenly',
		alignItems: 'center',
	},
	textInput: {
		height: 75,
		borderWidth: 1,
		borderRadius: 3,
		margin: 5,
		width: '80%',
		padding: 5,
	},
	createPathButton: {
		margin: 20,
		padding: 10,
		width: '50%',
		borderRadius: 3,
		backgroundColor: '#2196F3',
	},
	cancelPathButton: {
		margin: 20,
		padding: 10,
		width: '40%',
		borderRadius: 3,
		backgroundColor: '#6c757d',
	},
	validatePathButton: {
		margin: 20,
		padding: 10,
		width: '40%',
		borderRadius: 3,
		backgroundColor: '#28a745',
	},
	textButton: {
		textAlign: 'center',
		fontSize: 20,
		color: 'white',
	}
});