import React from 'react';
import { StyleSheet, View, Pressable, StatusBar } from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import { FontAwesome } from '@expo/vector-icons';
/***********************************************************************************************/
import ErrorManager from './errorManager';
import PathCreator from './PathCreator';
import * as GlobalFunction from './GlobalFunction';
import NewPathDisplayer from './NewPathDisplayer';
import PathDisplayer from './PathDisplayer';
import SelectedPath from './SelectedPath';
/***********************************************************************************************/

/**
 * Main screen
 */
export default class App extends React.Component {
	state = {
		latitude: 37.78825,		// Default values
		longitude: -122.4324,
		latitudeDelta: 0.009,
		longitudeDelta: 0.009,
		currentPath: [],		// Path currently created by the user
		allPath: [],
		isCreatingPath: false,
		pathId: "",
		selectedPath: null,
		errorMessage: "",
	};

	constructor(props) {
		super(props);
		// Init the allPath state
		this.updateAllPath();
	}

	// Try to get the location when the app is build.
	componentDidMount() {
		this.getLocationAsync();
	}

	// Clean the potential interval to avoid memory leak.
	componentWillUnmount() {
		clearInterval(this._interval);
	}

	// Update the location in the state variable.
	getLocationAsync = async () => {
		try {
			const { latitude, longitude, errorMessage } = await GlobalFunction.getLocationAsync();
			if (errorMessage == "") {
				this.setLocation(latitude, longitude);
			}
			this.setState({ errorMessage: errorMessage });
		} catch (err) {
			this.setState({
				errorMessage: err,
			});
		}
	}

	// Set the location (used by PathCreator)
	setLocation = (latitude, longitude, latitudeDelta = 0.009, longitudeDelta = 0.009) => {
		this.setState({ latitude: latitude, longitude: longitude, latitudeDelta: latitudeDelta, longitudeDelta: longitudeDelta });
	}

	// Return the path id (used by PathCreator)
	getPathId = () => {
		return this.state.pathId;
	}

	// Set the path id (used by PathCreator)
	setPathId = (pathId) => {
		this.setState({ pathId: pathId });
	}

	// Return the path currently created (used by NewPathDisplayer)
	getCurrentPath = () => {
		return this.state.currentPath;
	}

	// Add a new point to the currently created path (used by PathCreator and NewPathDisplayer)
	pushCurrentPath = (latitude, longitude) => {
		let currentPath = JSON.parse(JSON.stringify(this.state.currentPath));
		currentPath.push({ latitude: latitude, longitude: longitude });
		this.setState({ currentPath: currentPath });
	}

	// Reset the array of the currently created path (used by PathCreator)
	resetCurrentPath = () => {
		this.setState({ currentPath: [] });
	}

	// Return the boolean to know if the user is creating a path (used by NewPathDisplayer and PathDisplayer)
	getIsCreatingPath = () => {
		return this.state.isCreatingPath;
	}

	// Set the boolean to know if the user is creating a path (used by PathCreator)
	setIsCreatingPath = (value) => {
		this.setState({ isCreatingPath: value });
	}

	// Update the allPath state. Retrieve every locations, paths and comments.
	updateAllPath = () => {
		let pathsLocations = new Map();
		fetch(GlobalFunction.API_URL + "/locations/findall", {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
			}
		})
			.then(response => response.json()).then((responseJson) => {
				// Create a map with a format (key: path_id, value: {path_name, comment_message, null_array_of_coordinate})
				responseJson.forEach(paths => {
					pathsLocations.set(paths.fk_path, { name: paths.path.name, comment: paths.path.comment.message, coordinate: [] });
				});

				// Add the coordinate for each path
				pathsLocations.forEach(function (value, key) {
					responseJson.forEach(path => {
						if (path.fk_path == key) {
							value.coordinate.push({ longitude: path.longitude, latitude: path.latitude });
						}
					});
				});

				// The map is transformed into an Array (better to display it)
				this.setState({ allPath: Array.from(pathsLocations, ([key, value]) => ({ key, value })) });
			}).catch(error => console.log("Error : " + error))
	}

	// Return all the path stored (used by PathDisplayer)
	getAllPath = () => {
		return this.state.allPath;
	}

	// Return the information about the selected path (used by PahtCreator and PathDisplayer )
	getSelectedPath = () => {
		return this.state.selectedPath;
	}

	// Set information about the selected path (used by PathDisplayer and SelectedPath)
	setSelectedPath = (selectedPath) => {
		this.setState({ selectedPath: selectedPath });
	}

	// Main screen
	render() {
		const location = this.state
		return (
			<View style={styles.container}>
				<StatusBar backgroundColor="#ebffe7" barStyle='dark-content' />
				<MapView style={styles.map}
					region={location}>
					<Marker coordinate={location} title="Hej !" />
					<NewPathDisplayer getCurrentPath={this.getCurrentPath} />
					<PathDisplayer isCreatingPath={this.getIsCreatingPath} getAllPath={this.getAllPath}
						setSelectedPath={this.setSelectedPath} getSelectedPath={this.getSelectedPath}
						updateLocation={this.setLocation} />
				</MapView>
				<ErrorManager err={location.errorMessage} />
				<View style={styles.sideView}>
					<Pressable onPress={this.getLocationAsync} style={styles.button}>
						<FontAwesome style={styles.icons} name={'compass'} />
					</Pressable>
				</View>
				<PathCreator updateLocation={this.setLocation} updatePathId={this.setPathId} pathId={this.getPathId}
					pushCurrentPath={this.pushCurrentPath} resetCurrentPath={this.resetCurrentPath}
					updateAllPath={this.updateAllPath}
					getIsCreatingPath={this.getIsCreatingPath} setIsCreatingPath={this.setIsCreatingPath} />
				<SelectedPath getSelectedPath={this.getSelectedPath} setSelectedPath={this.setSelectedPath} />
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	map: {
		...StyleSheet.absoluteFillObject,
	},
	sideView: {
		alignItems: 'center',
		position: 'absolute',
		bottom: 115,
		right: 10,
		justifyContent: 'space-around',
	},
	button: {
		elevation: 5,
		borderRadius: 100,
		width: 50,
		height: 50,
		backgroundColor: 'white',
		justifyContent: 'center',
		alignItems: 'center'
	},
	icons: {
		fontSize: 30,
	}
});