import React from 'react';
import { StyleSheet, View, Pressable, Text } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

/**
 * View with information about the selected path
 */
export default class SelectedPath extends React.Component {

	// Close the view about the selected path
	closeSelectedPath = () => {
		// Reset the selected path and interval
		this.props.setSelectedPath(null);
		clearInterval(this.props.getSelectedPath()[0].interval);
	}

	// Render selected path info
	render() {
		pathInfo = this.props.getSelectedPath();
		if (pathInfo != null) {
			return (
				<View style={styles.bottomView}>
					<View style={styles.closeButton}>
						<Pressable onPress={this.closeSelectedPath} style={styles.button}>
							<FontAwesome style={styles.icons} name={'times'} />
						</Pressable>
					</View>
					<Text style={styles.title}>{"Information about this path :"}</Text>
					<Text style={styles.field}>
						<Text style={styles.label}>{"Name : "}</Text>
						<Text style={styles.content}>{pathInfo[0].value.name}</Text>
					</Text>
					<Text style={styles.field}>
						<Text style={styles.label}>{"Comment : "}</Text>
						<Text style={styles.content}>{pathInfo[0].value.comment}</Text>
					</Text>
					<Text style={styles.id}>{pathInfo[0].key}</Text>
				</View>
			);
		} else {
			return (null);
		}
	}
}

const styles = StyleSheet.create({
	closeButton: {
		alignItems: 'center',
		position: 'absolute',
		top: 5,
		right: 10,
		justifyContent: 'space-around',
	},
	button: {
		elevation: 15,
		borderRadius: 100,
		width: 40,
		height: 40,
		backgroundColor: 'lightgray',
		justifyContent: 'center',
		alignItems: 'center'
	},
	bottomView: {
		position: 'absolute',
		bottom: 0,
		width: '100%',
		backgroundColor: 'white',
		borderRadius: 3,
		elevation: 11,
	},
	title: {
		margin: 5,
		fontSize: 20,
		fontWeight: 'bold',
	},
	field: {
		margin: 5,
	},
	content: {
		fontSize: 20,
	},
	label: {
		fontSize: 20,
		fontStyle: 'italic',
	},
	id: {
		color: 'grey',
		fontSize: 10,
		bottom: 0,
	},
	icons: {
		fontSize: 20,
	}
});