# CrowdHike
CrowdHike is a mobile application for finding and creating pedestrian paths. As the name implies, the paths are created by users and the original idea was to allow users to find a walking path not always indicated on a map. However, it can also be used to define city walks.

The front-end is implemented in react native thanks to Expo and the back-end uses Sequelize to define the database.

## Front-end environment set up
> Before to try to set up your evironment please be sure to have at least **Node 12**.

We use **Expo** to run our project, so we need to install it.

```bash
sudo npm install -g expo-cli
```

You don't have to create an expo project since it is already include in this repository. We still need to install some packages in order to add some features in our app. To do so we will use the `package.json` and `package-lock.json`.

```bash
cd crowdhike/frontend
sudo npm ci
```

After that we can start the server to get a QR code that you can scan on your phone with the app Expo Go to see the app.

```bash
cd crowdhike/frontend
sudo expo start
```

> If you're working in a wsl you need to use a tunnel to access the server. You'll need another package and you can start the server like this : `sudo expo start --tunnel`

The main page of the app can be modified in the file crowdhike/frontend/App.js

### Communicate with the API
If the API is hosted locally you will need to create a tunnel to allow the app to communicate with it. For example, you can use localtunnel that you can install with the following command.
```bash
sudo npm install -g localtunnel
```

Then you have to start the API server and execute the following command :
```bash
lt --port <port_number>
```

This command will give you an url that you can use in the front to communicate with the API. Go to the *crowdhike/frontend/GlobalFunction.js* file and change the `API_URL` variable. For instance :
```js
export var API_URL = "https://cowardly-horse-38.loca.lt";
```

### Use the app on your phone
To use the app on your phone you can simply download the ExpoGo app on your phone and scan the QR code displayed after you executed the `sudo expo start` command.

## Back-end environment set up
In order to communicate with your application, you will need to install some node modules.

```bash
cd crowhike/src
sudo npm ci
```

### Set up your database
To run this project, you will need to set up a PostgreSQL database.
> Be sure to have at least **PostgreSQL 11**.

When your environment is installed, create a database and an associated user. From here run the following command from a Postgresql session.

```psql
SHOW hba_file;
```

This command returns the location of the configuration file `pg_hba.conf`, which you may need to edit in order to use `Sequelize`.


The end of your file should look like this:
```
...
# TYPE  DATABASE        USER            ADDRESS                 METHOD

# "local" is for Unix domain socket connections only
local   all             all                                     peer
# IPv4 local connections:
host    all             all             127.0.0.1/32            md5
# IPv6 local connections:
host    all             all             ::1/128                 md5
# Allow replication connections from localhost, by a user with the
# replication privilege.
local   replication     all                                     peer
host    replication     all             127.0.0.1/32            md5
host    replication     all             ::1/128                 md5
```
If you ever had the value ***ident** instead of **md5**, please make the change.

### Set up your .env file

In order to establish a connection between your API and the database, you need to set up an `.env` file in the `crowdhike/src` directory.
A template of the file is present in the `crowdhike/src/template/` directory, and looks like this.

```
usernameEnv=yourDBUsername
passwordEnv=yourDBPassword
hostEnv=localhost
databaseEnv=yourDBUsername
```

When all these operations have been done, you can start your server with the `sudo node .` command from the `crowdhike/src` directory.
